# Performance Evaluations

## Execution Times

- We are including execution times from two different machines.
- In both cases `-O3` argument is used during compilation.

### Nebula

- Nebula is a machine designed for HPC (High Performance Computing)
- The CPU has 16 cores with hyper threading to provide 32 threads, 
- The GPU has 3584 CUDA cores.
- OS: Ubuntu 16.04.6 LTS (GNU/Linux 4.4.0-159-generic x86_64)

![](nebula-exe-times.png)

### Personal Computer

- This is a typical desktop computer.
- The CPU has 6 cores.
- The GPU has 640 CUDA cores.
- OS: KDE neon 5.18 (Ubuntu 18.04 based) (GNU/Linux 5.3.0-51-generic x86_64)

![](personal-exe-times.png)

### With More Chromosomes

- To get decent improvements to the schedules a higher chromosome count is required.
- With single thread execution, we were unable to measure the time due to the extreme slowness.
- All results below are from the Nebula system described above.

![](nebula-1000chr.png)
![](nebula-speedup.png)


## Number of Generations

This is a more interesting metric as our main goal isn't to solve the problem
faster but get a better schedule in the same amount of time.

We tried including fitness score results but due to the randomness of the algorithm,
they were quite inconsistent. Generations therefore should provide a better metric.
In general a higher numbered generation should have better fitness scores.

![](nebula-generations.png)


