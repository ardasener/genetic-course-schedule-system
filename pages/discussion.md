# Discussion

## Comment on The Results

As we suspected, course scheduling was a good area for the use of a genetic algorithm
and the genetic algorithm was a good candidate for parallelization. 

## Known Bugs

There is a very rare bug where one of the random indices exceed the size of the schedule.
It is not known whether this issue is due to our genetic algorithm or one of the external libraries used.
We are unable to reproduce this issue consistently and as a result we were unable to debug it or trace its source.
As stated, this is extremely rare and should not be of issue in most cases.

## Future Work

- Some constraints were not implemented, like instructors having too many classes in one day.
- More work is required to get performance improvements from the hybrid parallelism implementation.
- More configuration and input options might be added:
	- Fixed classes which can not be altered during mutations and crossovers
	- Custom constraints
